# TP6-conception-logicielle

Bonjour, avec cette application, vous allez pouvoir détecter si un produit est vegan ou non à partir de son code-barre. 
## Installer les dépendances
Installer les dépendances nécessaires à exécuter le code, tapez cette ligne dans le terminal : 
```pip install -r requirements.txt```

## Lancer l'application
Taper dans votre terminal : 
```uvicorn main:app --reload```

## Cliquer sur le lien http de la sortie

1. Ajouter à la fin de l'adresse http : /docs#

## Ajout de mots 

1. Cliquez sur *Ajout mot*
2. Cliquez sur *Try it out*
3. Insérer le mot que vous voulez ajouter ainsi qu'un id
4. Cliquez sur *Execute*

## Visualiser la liste de mots

1. Cliquez sur *Visualiser Liste Mots*
2. Cliquez sur *Try it out*
3. Cliquez sur *Execute*

