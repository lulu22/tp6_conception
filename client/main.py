import requests
from pydantic import BaseModel

class Mot(BaseModel):
    id:int
    caracteres:str

def gets_mots():
    requete = requests.get("http://127.0.0.1:8000/mots")
    return requete.json()

def add_mots(id, mot):
    mon_mot = {"id" : id, "caracteres":mot}
    requete = requests.post("http://127.0.0.1:8000/mot", json = mon_mot)
    return requete.json()

print(gets_mots())
print(add_mots(6, "mangue"))