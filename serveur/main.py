from random import random
from turtle import title
from typing import Optional
from typing import List
from fastapi import FastAPI
from mot import Mot
from guess import Guess

app = FastAPI(title = "Jeu du pendu", description = "C'est le jeu du pendu")
liste_mots:List[Mot] = []
liste_guess:List[Guess] = []

@app.get("/init")
async def recup_premier_guess():
    guess = Guess(mot = liste_mots[random.randit(0, len(liste_mots))], erreur = 0, letter = "")
    return guess

@app.post("/guess")
async def test_guess(guess:Guess):
    if guess.letter in guess.mot:
        pass
    else:
        guess.erreurs += 1
    return guess

@app.get("/mot")
async def visualiser_liste_mots():
    return liste_mots

@app.post("/mot")
async def ajout_mot(mot: Mot):
    liste_mots.append(mot)
    return liste_mots

